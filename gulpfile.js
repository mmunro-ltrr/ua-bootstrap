'use strict'

const gulp = require('gulp')
const config = require('./tasks/config')

function loadTask (fileName, taskName) {
  const taskModule = require('./tasks/' + fileName)
  const task = taskName ? taskModule[taskName] : taskModule
  return task(gulp, config)
}

/* Default Task */
gulp.task('default', ['clean', 'build'])

/* Live preview server for Dev */
gulp.task('serve', ['default'], loadTask('serve', 'start'))

/* Build Tasks */
gulp.task('build', ['minify', 'docs'])
gulp.task('build:dist', ['clean:dist', 'default'], loadTask('build', 'dist'))

/* Clean */
gulp.task('clean', loadTask('clean', 'build'))
gulp.task('clean:dist', ['clean'], loadTask('clean', 'dist'))

/* Minification */
gulp.task('minify', ['minify:css', 'minify:js'])
gulp.task('minify:css', ['sass'], loadTask('stylesheets', 'minify'))
gulp.task('minify:js', ['js'], loadTask('javascripts', 'minify'))

/* Stylesheets */
gulp.task('sass', ['remove-glyphicons', 'svg', 'img'], loadTask('stylesheets', 'sass'))
gulp.task('remove-glyphicons', loadTask('stylesheets', 'removeGlyphicons'))
gulp.task('svg', loadTask('stylesheets', 'svg'))
gulp.task('img', loadTask('stylesheets', 'img'))

/* JavaScripts */
gulp.task('js', loadTask('javascripts', 'js'))

/* Documentation */
gulp.task('docs', ['minify', 'docs:pug', 'docs:assets'])
gulp.task('docs:pug', loadTask('docs', 'pug'))
gulp.task('docs:assets', loadTask('docs', 'assets'))

/* Testing */
gulp.task('test', ['test:reference'], loadTask('test', 'run'))
gulp.task('test:reference', ['build'], loadTask('test', 'reference'))
gulp.task('test:run', loadTask('test', 'run'))
gulp.task('test:pipeline', loadTask('test', 'pipeline'))
