## Want to Contribute?

<p class="lead">Any release of UA Bootstrap is tested and ready to use. But improvements to the framework are in everyone's best interest.</p>

A team of web-focused volunteers known as UA Digital meets weekly to build and
test products like UA Bootstrap and <a href="https://brand.arizona.edu/guide/ua-quickstart" target="_blank">UA
Quickstart</a>.

### **Get Involved**

- Request an invite to the Friday UA Digital meetings by subscribing to the <a href="https://list.arizona.edu/sympa/info/ua-digital" target="_blank">UA
  Digital listserv</a>
- Join the UA Digital discussions on <a href="https://uarizona.slack.com/messages/ua-bootstrap" target="_blank">Slack</a>
- Submit pull requests on <a href="https://bitbucket.org/uadigital/ua-bootstrap" target="_blank">Bitbucket</a>

Questions, bugs, or suggestions can also be emailed to
[brand@email.arizona.edu](mailto:brand@email.arizona.edu?subject=UA Bootstrap
Feedback')
