<h1 id="thumbnails">Thumbnails</h1>
<p class="lead">Extend Bootstrap's <a href="../css.html#grid">grid system</a> with the thumbnail component to easily display grids of images, videos, text, and more.</p>
<p>If you're looking for Pinterest-like presentation of thumbnails of varying heights and/or widths, you'll need to use a third-party plugin such as <a href="http://masonry.desandro.com" target="_blank">Masonry</a>, <a href="http://isotope.metafizzy.co" target="_blank">Isotope</a>, or <a href="http://salvattore.com" target="_blank">Salvattore</a>.</p>
<p>If you're looking for a specifically designed Photo Gallery feature see <a href="javascript.html#gallery">The UAQS Photo Gallery</a>.</p>
<h3 id="thumbnails-default">Default example</h3>
<p>By default, Bootstrap's thumbnails are designed to showcase linked images with minimal required markup.</p>

<div class="example" data-example-id="simple-thumbnails">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="row">
    <div class="col-xs-6 col-md-3">
      <a href="#" class="thumbnail">
        <img data-src="holder.js/100px180" alt="Generic placeholder thumbnail">
      </a>
    </div>
    <div class="col-xs-6 col-md-3">
      <a href="#" class="thumbnail">
        <img data-src="holder.js/100px180" alt="Generic placeholder thumbnail">
      </a>
    </div>
    <div class="col-xs-6 col-md-3">
      <a href="#" class="thumbnail">
        <img data-src="holder.js/100px180" alt="Generic placeholder thumbnail">
      </a>
    </div>
    <div class="col-xs-6 col-md-3">
      <a href="#" class="thumbnail">
        <img data-src="holder.js/100px180" alt="Generic placeholder thumbnail">
      </a>
    </div>
  </div>
</div>

```html
<div class="row">
  <div class="col-xs-6 col-md-3">
    <a href="#" class="thumbnail">
      <img src="..." alt="...">
    </a>
  </div>
  ...
</div>
```

<h3 id="thumbnails-custom-content">Custom content</h3>
<p>With a bit of extra markup, it's possible to add any kind of HTML content like headings, paragraphs, or buttons into thumbnails.</p>

<div class="example" data-example-id="thumbnails-with-custom-content">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="thumbnail">
        <img data-src="holder.js/100px200" alt="Generic placeholder thumbnail">
        <div class="caption">
          <h3>Thumbnail label</h3>
          <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
          <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="thumbnail">
        <img data-src="holder.js/100px200" alt="Generic placeholder thumbnail">
        <div class="caption">
          <h3>Thumbnail label</h3>
          <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
          <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-md-4">
      <div class="thumbnail">
        <img data-src="holder.js/100px200" alt="Generic placeholder thumbnail">
        <div class="caption">
          <h3>Thumbnail label</h3>
          <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
          <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
        </div>
      </div>
    </div>
  </div>
</div>

```html
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="..." alt="...">
      <div class="caption">
        <h3>Thumbnail label</h3>
        <p>...</p>
        <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
      </div>
    </div>
  </div>
</div>
```

