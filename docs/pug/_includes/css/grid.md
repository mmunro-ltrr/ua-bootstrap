<a id="grid"></a>
## Grid system

<p class='lead'>
Bootstrap includes a responsive, mobile first fluid grid system that
appropriately scales up to 12 columns as the device or viewport size increases.
It includes **[predefined classes](#grid-example-basic)** for easy layout options.</p>

<a id="grid-intro"></a>
### Introduction

Grid systems are used for creating page layouts through a series of rows and
columns that house your content. Here's how the Bootstrap grid system works:

- Rows must be placed within a `.container` (fixed-widt) or `.container-fluid`
  (full-width) for proper alignment and padding.
- Use rows to create horizontal groups of columns.
- Content should be placed within columns, and only columns may be immediate
  children of rows.
- Predefined grid classes like `.row` and `.col-xs-4` are available for quickly
  making grid layouts.
- Columns create gutters (gaps between column content) via `padding`. That
  padding is offset in rows for the first and last column via negative margin
  with elements with a class of `.row`.
- The negative margin is why the examples below are outdented. It's so that
  content within grid columns is lined up with non-grid content.
- Grid columns are created by specifying the number of twelve available columns
  you wish to span. For example, three equal columns would use three
  `.col-xs-4.`
- If more than 12 columns are placed within a single row, each group of extra
  columns will, as one unit, wrap onto a new line.
- Grid classes apply to devices with screen widths greater than or equal to
  the breakpoint sizes, and override grid classes targeted at smaller devices.
  Therefore, e.g. applying any `.col-md-*` class to an element will not only
  affect its styling on medium devices but also on large devices if
  a `.col-lg-*` class is not present.

Look to the examples for applying these principles to your code.

### Grid options

See how aspects of the Bootstrap grid system work across multiple devices with
a handy table.

<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Extra small devices<small><br>Phones (&lt;768px)</small></th>
                <th>Small devices<small><br>Tablets (≥768px)</small></th>
                <th>Medium devices<small><br>Desktops (≥992px)</small></th>
                <th>Large devices<small><br>Desktops (≥1200px)</small></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row" class="text-nowrap">Grid behavior</th>
                <td>Horizontal at all times</td>
                <td colspan="3">Collapsed to start, horizontal above breakpoints</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Container width</th>
                <td>None (auto)</td>
                <td>752px</td>
                <td>972px</td>
                <td>1172px</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Class prefix</th>
                <td><code>.col-xs-</code></td>
                <td><code>.col-sm-</code></td>
                <td><code>.col-md-</code></td>
                <td><code>.col-lg-</code></td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap"># of columns</th>
                <td colspan="4">12</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Column width</th>
                <td class="text-muted">Auto</td>
                <td>60px</td>
                <td>~78px</td>
                <td>95px</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Gutter width</th>
                <td colspan="4">32px (16px on each side of a column)</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Nestable</th>
                <td colspan="4">Yes</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Offsets</th>
                <td colspan="4">Yes</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Column ordering</th>
                <td colspan="4">Yes</td>
            </tr>
        </tbody>
    </table>
</div>

<!--<a id="example-stacked-to-horizontal"></a>-->
### Example: Stacked-to-horizontal

Using a single set of `.col-md-*` grid classes, you can create a basic grid
system that stars out stacked on mobile devices and tablet devices (the extra
small to small range) before becoming horizontal on desktop (medium) devices.
Place grid columns in any `.row`.
<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <div class="row show-grid">
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
 </div>
 <div class="row show-grid">
  <div class="col-md-8">.col-md-8</div>
  <div class="col-md-4">.col-md-4</div>
 </div>
 <div class="row show-grid">
  <div class="col-md-4">.col-md-4</div>
  <div class="col-md-4">.col-md-4</div>
  <div class="col-md-4">.col-md-4</div>
 </div>
 <div class="row show-grid">
  <div class="col-md-6">.col-md-6</div>
  <div class="col-md-6">.col-md-6</div>
 </div>
</div>

```html
<div class="row">
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
</div>
<div class="row">
     <div class="col-md-8">.col-md-8</div>
     <div class="col-md-4">.col-md-4</div>
</div>
<div class="row">
    <div class="col-md-4">.col-md-4</div>
    <div class="col-md-4">.col-md-4</div>
    <div class="col-md-4">.col-md-4</div>
</div>
<div class="row">
    <div class="col-md-6">.col-md-6</div>
    <div class="col-md-6">.col-md-6</div>
</div>
```

### Example: Fluid container

Turn any fixed-width grid layout into a full-width layout by changing your
outermost `.container` to `.container-fluid`

```html
<div class="container-fluid">
  <div class="row">
      ...
  </div>
</div>
```

<a id="grid-example-mixed"></a>
### Example: Mobile and desktop

Don&apos;t want your columns to simply stack in smaller devices? Use the extra
small and medium device grid classes by adding `.col-xs-*` `.col-md-*` to your
columns. See the example below for a better idea of how it all works.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <div class="row show-grid">
  <div class="col-xs-12 col-md-8">.col-xs-12 .col-md-8</div>
  <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
 </div>

 <div class="row show-grid">
  <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
  <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
  <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
 </div>

 <div class="row show-grid">
  <div class="col-xs-6">.col-xs-6</div>
  <div class="col-xs-6">.col-xs-6</div>
 </div>
</div>

```html
<!-- Stack the columns on mobile by making one full-width and the other half-width -->
<div class="row">
 <div class="col-xs-12 col-md-8">.col-xs-12 .col-md-8</div>
 <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
</div>

<!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
<div class="row">
 <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
 <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
 <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
</div>

<!-- Columns are always 50% wide, on mobile and desktop -->
<div class="row">
 <div class="col-xs-6">.col-xs-6</div>
 <div class="col-xs-6">.col-xs-6</div>
</div>
```

<a id="grid-example-mixed-complete"></a>
### Example: Mobile, tablet, desktop

Build on the previous example by creating even more dynamic and powerful layouts
with tablet `.col-sm-*` classes.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <div class="row show-grid">
     <div class="col-xs-12 col-sm-6 col-md-8">.col-xs-12 .col-sm-6 .col-md-8</div>
     <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
 </div>
 <div class="row show-grid">
     <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
     <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
     <!-- Optional: clear the XS cols if their content doesn't match in height -->
     <div class="clearfix visible-xs-block"></div>
     <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
 </div>
</div>

```html
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-8">.col-xs-12 .col-sm-6 .col-md-8</div>
    <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
</div>
<div class="row">
    <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
    <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
    <!-- Optional: clear the XS cols if their content doesn't match in height -->
    <div class="clearfix visible-xs-block"></div>
    <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
</div>
```

<a id="grid-example-wrapping"></a>
### Example: Column wrapping

If more than 12 columns are placed within a single row, each group of extra
columns will, as one unit, wrap onto a new line.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <div class="row show-grid">
   <div class="col-xs-9">.col-xs-9</div>
   <div class="col-xs-4">.col-xs-4<br>Since 9 + 4 = 13 > 12, this 4-column-wide div gets wrapped onto a new line as one contiguous unit.</div>
   <div class="col-xs-6">.col-xs-6<br>Subsequent columns continue along the new line.</div>
 </div>
</div>

```html
<div class="row">
  <div class="col-xs-9">.col-xs-9</div>
  <div class="col-xs-4">.col-xs-4<br>Since 9 + 4 = 13 > 12, this 4-column-wide div gets wrapped onto a new line as one contiguous unit.</div>
  <div class="col-xs-6">.col-xs-6<br>Subsequent columns continue along the new line.</div>
</div>
```

<!--<a id="grid-responsive-resets"></a>-->
### Responsive column resets

With the four tiers of grids available you're bound to run into issues where, at
certain breakpoints, your columns don't clear quite right as one is taller than
the other. To fix that, use a combination of a `.clearfix` and our [responsive
utility classes](#responsive-utilities).

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <div class="row show-grid">
  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>
  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>
  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>
 </div>
</div>

```html
<div class="row">
  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>
  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>
  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>
</div>
```

In addition to column clearing at responsive breakpoints, you may need to
**reset offsets, pushes, or pulls**. See this in action in the [grid example](/grids.html).

<!--<a id="grid-offsetting"></a>-->
### Offsetting columns

Move columns to the right using `.col-md-offset-*` classes. These classes
increase the left margin of a column by `*` columns. For example,
`.col-md-offset-4` moves `.col-md-4` over four columns.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <div class="row show-grid">
  <div class="col-sm-5 col-md-6">.col-sm-5 .col-md-6</div>
  <div class="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-0">.col-sm-5 .col-sm-offset-2 .col-md-6 .col-md-offset-0</div>
 </div>

 <div class="row show-grid">
  <div class="col-sm-6 col-md-5 col-lg-6">.col-sm-6 .col-md-5 .col-lg-6</div>
  <div class="col-sm-6 col-md-5 col-md-offset-2 col-lg-6 col-lg-offset-0">.col-sm-6 .col-md-5 .col-md-offset-2 .col-lg-6 .col-lg-offset-0</div>
 </div>
</div>

```html
<div class="row">
    <div class="col-sm-5 col-md-6">.col-sm-5 .col-md-6</div>
    <div class="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-0">.col-sm-5 .col-sm-offset-2 .col-md-6 .col-md-offset-0</div>
</div>

<div class="row">
    <div class="col-sm-6 col-md-5 col-lg-6">.col-sm-6 .col-md-5 .col-lg-6</div>
    <div class="col-sm-6 col-md-5 col-md-offset-2 col-lg-6 col-lg-offset-0">.col-sm-6 .col-md-5 .col-md-offset-2 .col-lg-6 .col-lg-offset-0</div>
</div>
```

<!--<a id="grid-nesting"></a>-->
### Nesting columns

To nest your content with the default grid, add a new `.row` and set of
`.col-sm-*` columns within an existing `.col-sm-*` column. Nested rows should
include a set of columns that add up to 12 or fewer (it is not required that you
use all 12 available columns).

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <div class="row show-grid">
  <div class="col-sm-9">
   Level 1: .col-sm-9
   <div class="row show-grid">
   <div class="col-xs-8 col-sm-6">
     Level 2: .col-xs-8 .col-sm-6
   </div>
   <div class="col-xs-4 col-sm-6">
     Level 2: .col-xs-4 .col-sm-6
   </div>
   </div>
  </div>
 </div>
</div>

```html
<div class="row">
 <div class="col-sm-9">
   Level 1: .col-sm-9
   <div class="row">
     <div class="col-xs-8 col-sm-6">
       Level 2: .col-xs-8 .col-sm-6
     </div>
     <div class="col-xs-4 col-sm-6">
       Level 2: .col-xs-4 .col-sm-6
     </div>
   </div>
 </div>
</div>
```

<!--<a id="grid-column-ordering"></a>-->
### Column ordering

Easily change the order of our built-in grid columns with `.col-md-push-*` and
`.col-md-pull-*` modifier classes.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <div class="row show-grid">
  <div class="col-md-9 col-md-push-3">.col-md-9 .col-md-push-3</div>
  <div class="col-md-3 col-md-pull-9">.col-md-3 .col-md-pull-9</div>
</div>
</div>

```html
<div class="row">
  <div class="col-md-9 col-md-push-3">.col-md-9 .col-md-push-3</div>
  <div class="col-md-3 col-md-pull-9">.col-md-3 .col-md-pull-9</div>
</div>
```
### Spacing

Spacing utilities that apply to all breakpoints, from `xs` to `lg`, have no breakpoint abbreviation in them. This is because those classes are applied from `min-width: 0` and up, and thus are not bound by a media query. The remaining breakpoints, however, do include a breakpoint abbreviation.

The classes are named using the format `{property}{sides}-{size}` for `xs` and `{property}{sides}-{breakpoint}-{size}` for `sm`, `md`, and `lg`.

Where _property_ is one of:

* `m` - for classes that set `margin`
* `p` - for classes that set `padding`

Where _sides_ is one of:

* `t` - for classes that set `margin-top` or `padding-top`
* `b` - for classes that set `margin-bottom` or `padding-bottom`
* `l` - for classes that set `margin-left` or `padding-left`
* `r` - for classes that set `margin-right` or `padding-right`
* `x` - for classes that set both `*-left` and `*-right`
* `y` - for classes that set both `*-top` and `*-bottom`
* blank - for classes that set a `margin` or `padding` on all 4 sides of the element

Where _size_ is one of:

* `0` - for classes that eliminate the `margin` or `padding` by setting it to `0`
* `min` - (by default) for classes that set the `margin` or `padding` to .0625rem (~1px)
* `1` - (by default) for classes that set the `margin` or `padding` to .25rem (~4px)
* `2` - (by default) for classes that set the `margin` or `padding` to .5rem (~8px)
* `3` - (by default) for classes that set the `margin` or `padding` to 1rem (~16px)
* `4` - (by default) for classes that set the `margin` or `padding` to 1.5rem (~24px)
* `5` - (by default) for classes that set the `margin` or `padding` to 3rem (~48px)
* `6` - (by default) for classes that set the `margin` or `padding` to 4rem (~64px)
* `7` - (by default) for classes that set the `margin` or `padding` to 5rem (~80px)
* `8` - (by default) for classes that set the `margin` or `padding` to 6rem (~96px)
* `9` - (by default) for classes that set the `margin` or `padding` to 7rem (~112px)
* `10` - (by default) for classes that set the `margin` or `padding` to 8rem (~128px)
</br>**NOTE**: Size is set using the variable `$spacer` which by default is set to 1rem. All margin/padding sizes can be changed by editing this variable.

<div class="bs-callout bs-callout-warning" id="">
  It is important to note the differences between `margin` and `padding`. The single biggest Difference is that **vertical** margins auto-collapse, and padding doesn't.
</div>
<div class="example show-grid text-center">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <h5 class="text-left">Margins</h5>
  <div class="row">
    <div class="col-xs-12 my-2">This is a div with both top and bottom margins set to **16px**</div>
  </div>
  <div class="row">
    <div class="col-xs-12 my-4">This is a div with both top and bottom margins set to **24px**</div>
  </div>
  <div class="row">
    <div class="col-xs-12 my-8">This is a div with both top and bottom margins set to **96px**</div>
  </div>
  <h5 class="text-left">Padding</h5>
  <div class="row">
    <div class="col-xs-12 py-2">This is a div with both top and bottom padding set to **8px**</div>
  </div>
  <div class="row">
    <div class="col-xs-12 py-4">This is a div with both top and bottom padding set to **24px**</div>
  </div>
  <div class="row">
    <div class="col-xs-12 py-8">This is a div with both top and bottom padding set to **96px**</div>
  </div>
</div>

```html
<h5 class="text-left">Margins</h5>
<div class="row">
  <div class="col-xs-12 my-2">This is a div with both top and bottom margins set to 16px</div>
</div>
<div class="row">
  <div class="col-xs-12 my-4">This is a div with both top and bottom margins set to 24px</div>
</div>
<div class="row">
  <div class="col-xs-12 my-8">This is a div with both top and bottom margins set to 96px</div>
</div>
<h5 class="text-left">Padding</h5>
<div class="row">
  <div class="col-xs-12 py-2">This is a div with both top and bottom padding set to 8px</div>
</div>
<div class="row">
  <div class="col-xs-12 py-4">This is a div with both top and bottom padding set to 24px</div>
</div>
<div class="row">
  <div class="col-xs-12 py-8">This is a div with both top and bottom padding set to 96px</div>
</div>
```

#### Usage
```css
.mt-0 { 
  margin-top: 0 !important;
}
.ml-1 { 
  margin-left: 4px !important;
  margin-left: ($spacer * .25);
    }
.px-2 {
  padding-right: 8px !important;
  padding-right: ($spacer * .5) !important; 
  padding-left: 8px !important;
  padding-left: ($spacer * .5) !important; 
}
.p-3 {
  padding: 16px !important;
  padding: $spacer !important;
}
```

### Horizontal Centering

UA-Bootstrap now includes Bootstrap 4's `.mx-auto` class for horizontally centering fixed-width block level content.
That is, content that has `display: block` and a `width` set.
It does this by setting the horizontal margins to `auto`

<div class="example">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="row show-grid">
    <div class="mx-auto" style="width:200px;"><a class="btn btn-default btn-block">Centered Element</a></div>
  </div>
</div>

```html
<div class="row show-grid">
  <div class="mx-auto" style="width:200px;"><a class="btn btn-default btn-block">Centered Element</a></div>
</div>
```

<!--<a id="grid-row-buffer"></a>-->
### Row buffer

<div class="bs-callout bs-callout-danger" id="">
  <h4>Deprecation Warning</h4>
  <p>WARNING: Buffer classes have been deprecated in favor of spacing classes (See <a href="#spacing">Spacing</a>)</p>
</div>

In order to allow the typography to set the vertical rhythm, rows do not have any margins around them by default. These classes are intended to be used sparingly only when default vertical rhythm appears unnatural: `.top-buffer-md-*` or `.bottom-buffer-md-*` .

This adds a series of buffer classes, and buffer resets.
The buffers follow the pattern of `SIDE-buffer-SIZE-*`
The resets follow the pattern of `SIDE-buffer-SIZE-reset`(excluding xs)

SIDE referes to which side of the element you would like the buffer class to be applied to (top/bottom/left/right).

SIZE referes to the viewport(s) you would like the buffer classes applied to. The viewports use the (xs/sm/md/lg) naming scheme.

NOTE: Buffer classes will be applied to the viewport you specify, as well as any 'larger' viewports.

For example: If I want to apply a 5px top buffer to an element, but I want it applied on only xs and sm viewports I would use:
`top-buffer-xs-5` AND
`top-buffer-md-reset`

That will apply a top buffer of 5px to my element at the xs viewport and up, but then reset the buffer of the element on md viewport and up.

<div class="bs-callout bs-callout-warning" id="">
  <h4>Additional bottom buffer classes added</h4>
  <p>Initially <code>.bottom-buffer-\*</code> was added to bootstrap but now additional classes have been added to help positioning on different viewports for example <code>.bottom-buffer-md-\*</code> </p>
</div>

<div class="example">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <h5>Top Buffer</h5>
  <div class="row show-grid">
    <div class="col-xs-12">top-buffer-\*-0 or top-buffer-\*-reset, default buffer is
  zero. You can also just leave off if not removing margin.</div>
  </div>
  <div class="row show-grid top-buffer-xs-1">
    <div class="col-xs-12">top-buffer-\*-1</div>
  </div>
  <div class="row show-grid top-buffer-xs-5">
    <div class="col-xs-12">top-buffer-\*-5</div>
  </div>
  <div class="row show-grid top-buffer-xs-10">
    <div class="col-xs-12">top-buffer-\*-10</div>
  </div>
  <div class="row show-grid top-buffer-xs-15">
    <div class="col-xs-12">top-buffer-\*-15</div>
  </div>
  <div class="row show-grid top-buffer-xs-20">
    <div class="col-xs-12">top-buffer-\*-20</div>
  </div>
  <div class="row show-grid top-buffer-xs-25">
    <div class="col-xs-12">top-buffer-\*-25</div>
  </div>
  <div class="row show-grid top-buffer-xs-30">
    <div class="col-xs-12">top-buffer-\*-30</div>
  </div>
  <div class="row show-grid top-buffer-xs-50">
    <div class="col-xs-12">top-buffer-\*-50</div>
  </div>
</div>

```html
<div class="row">
  <div class="col-xs-12">top-buffer-*-0 or top-buffer-*-reset, default buffer is
  zero. You can also just leave off if not removing margin.</div>
</div>
<div class="row top-buffer-xs-1">
  <div class="col-xs-12">top-buffer-*-1</div>
</div>
<div class="row top-buffer-xs-5">
  <div class="col-xs-12">top-buffer-*-5</div>
</div>
<div class="row top-buffer-xs-10">
  <div class="col-xs-12">top-buffer-*-10</div>
</div>
<div class="row top-buffer-xs-15">
  <div class="col-xs-12">top-buffer-*-15</div>
</div>
<div class="row top-buffer-xs-20">
  <div class="col-xs-12">top-buffer-*-20</div>
</div>
<div class="row top-buffer-xs-25">
  <div class="col-xs-12">top-buffer-*-25</div>
</div>
<div class="row top-buffer-xs-30">
  <div class="col-xs-12">top-buffer-*-30</div>
</div>
<div class="row top-buffer-xs-50">
  <div class="col-xs-12">top-buffer-*-50</div>
</div>
```


<div class="example">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <h5>Bottom Buffer</h5>
  <div class="row show-grid">
    <div class="col-xs-12">bottom-buffer-\*-0 or bottom-buffer-\*-reset, default buffer is
  zero. You can also just leave off if not removing margin.</div>
  </div>
  <div class="row show-grid bottom-buffer-xs-1">
    <div class="col-xs-12">bottom-buffer-\*-1</div>
  </div>
  <div class="row show-grid bottom-buffer-xs-5">
    <div class="col-xs-12">bottom-buffer-\*-5</div>
  </div>
  <div class="row show-grid bottom-buffer-xs-10">
    <div class="col-xs-12">bottom-buffer-\*-10</div>
  </div>
  <div class="row show-grid bottom-buffer-xs-15">
    <div class="col-xs-12">bottom-buffer-\*-15</div>
  </div>
  <div class="row show-grid bottom-buffer-xs-20">
    <div class="col-xs-12">bottom-buffer-\*-20</div>
  </div>
  <div class="row show-grid bottom-buffer-xs-25">
    <div class="col-xs-12">bottom-buffer-\*-25</div>
  </div>
  <div class="row show-grid bottom-buffer-xs-30">
    <div class="col-xs-12">bottom-buffer-\*-30</div>
  </div>
  <div class="row show-grid bottom-buffer-xs-50">
    <div class="col-xs-12">bottom-buffer-\*-50</div>
  </div>
</div>

```html
<div class="row">
  <div class="col-xs-12">bottom-buffer-*-0 or bottom-buffer-*-reset, default buffer is
  zero. You can also just leave off if not removing margin.</div>
</div>
<div class="row bottom-buffer-xs-1">
  <div class="col-xs-12">bottom-buffer-*-1</div>
</div>
<div class="row bottom-buffer-xs-5">
  <div class="col-xs-12">bottom-buffer-*-5</div>
</div>
<div class="row bottom-buffer-xs-10">
  <div class="col-xs-12">bottom-buffer-*-10</div>
</div>
<div class="row bottom-buffer-xs-15">
  <div class="col-xs-12">bottom-buffer-*-15</div>
</div>
<div class="row bottom-buffer-xs-20">
  <div class="col-xs-12">bottom-buffer-*-20</div>
</div>
<div class="row bottom-buffer-xs-25">
  <div class="col-xs-12">bottom-buffer-*-25</div>
</div>
<div class="row bottom-buffer-xs-30">
  <div class="col-xs-12">bottom-buffer-*-30</div>
</div>
<div class="row bottom-buffer-xs-50">
  <div class="col-xs-12">bottom-buffer-*-50</div>
</div>
```

<!--<a id="grid-column-buffer"></a>-->
### Column buffer

<div class="bs-callout bs-callout-danger" id="">
  <h4>Deprecation Warning</h4>
  <p>WARNING: Buffer classes have been deprecated in favor of spacing classes (See <a href="#spacing">Spacing</a>)</p>
</div>

This adds a series of buffer classes, and buffer resets.
The buffers follow the pattern of `SIDE-buffer-SIZE-*`
The resets follow the pattern of `SIDE-buffer-SIZE-reset` (excluding xs)

SIDE referes to which side of the element you would like the buffer class to be applied to (top/bottom/left/right).

SIZE referes to the viewport(s) you would like the buffer classes applied to. The viewports use the (xs/sm/md/lg) naming scheme.

NOTE: Buffer classes will be applied to the viewport you specify, as well as any 'larger' viewports.

For example: If I want to apply a 5px top buffer to an element, but I want it applied on only xs and sm viewports I would use:
`top-buffer-xs-5` AND
`top-buffer-md-reset`

That will apply a top buffer of 5px to my element at the xs viewport and up, but then reset the buffer of the element on md viewport and up.

With the `.right-buffer-*-*` or `.left-buffer-*-*` class, you can add buffer to the right and left of columns.

<div class="example">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="row show-grid bottom-buffer-xs-10">
    <div class="col-xs-6 right-buffer-xs-0"><a class="btn btn-default btn-block">right-buffer-\*-0</a></div>
    <div class="col-xs-6 left-buffer-xs-0"><a class="btn btn-default btn-block">left-buffer-\*-0</a></div>
  </div>
  <div class="row show-grid bottom-buffer-xs-10">
    <div class="col-xs-6 right-buffer-xs-1"><a class="btn btn-default btn-block">right-buffer-\*-1</a></div>
    <div class="col-xs-6 left-buffer-xs-1"><a class="btn btn-default btn-block">left-buffer-\*-1</a></div>
  </div>
  <div class="row show-grid bottom-buffer-xs-10">
    <div class="col-xs-6 right-buffer-xs-5"><a class="btn btn-default btn-block">right-buffer-\*-5</a></div>
    <div class="col-xs-6 left-buffer-xs-5"><a class="btn btn-default btn-block">left-buffer-\*-5</a></div>
  </div>
  <div class="row show-grid bottom-buffer-xs-10">
    <div class="col-xs-6 right-buffer-xs-10"><a class="btn btn-default btn-block">right-buffer-\*-10</a></div>
    <div class="col-xs-6 left-buffer-xs-10"><a class="btn btn-default btn-block">left-buffer-\*-10</a></div>
  </div>
  <div class="row show-grid bottom-buffer-xs-10">
    <div class="col-xs-6 right-buffer-xs-15"><a class="btn btn-default btn-block">right-buffer-\*-15</a></div>
    <div class="col-xs-6 left-buffer-xs-15"><a class="btn btn-default btn-block">left-buffer-\*-15</a></div>
  </div>
  <div class="row show-grid bottom-buffer-xs-10">
    <div class="col-xs-6 right-buffer-xs-20"><a class="btn btn-default btn-block">right-buffer-\*-20</a></div>
    <div class="col-xs-6 left-buffer-xs-20"><a class="btn btn-default btn-block">left-buffer-\*-20</a></div>
  </div>
  <div class="row show-grid bottom-buffer-xs-10">
    <div class="col-xs-6 right-buffer-xs-30"><a class="btn btn-default btn-block">right-buffer-\*-30</a></div>
    <div class="col-xs-6 left-buffer-xs-30"><a class="btn btn-default btn-block">left-buffer-\*-30</a></div>
  </div>
</div>

```html
<div class="row bottom-buffer-xs-10">
  <div class="col-xs-6 right-buffer-xs-0">
    <a class="btn btn-default btn-block">right-buffer-*-0</a>
  </div>
  <div class="col-xs-6 left-buffer-xs-0">
    <a class="btn btn-default btn-block">left-buffer-*-0</a>
  </div>
</div>
<div class="row bottom-buffer-xs-10">
  <div class="col-xs-6 right-buffer-xs-1">
    <a class="btn btn-default btn-block">right-buffer-*-1</a>
  </div>
  <div class="col-xs-6 left-buffer-xs-1">
    <a class="btn btn-default btn-block">left-buffer-*-1</a>
  </div>
</div>
<div class="row bottom-buffer-xs-10">
  <div class="col-xs-6 right-buffer-xs-5">
    <a class="btn btn-default btn-block">right-buffer-*-5</a>
  </div>
  <div class="col-xs-6 left-buffer-xs-5">
    <a class="btn btn-default btn-block">left-buffer-*-5</a>
  </div>
</div>
<div class="row bottom-buffer-xs-10">
  <div class="col-xs-6 right-buffer-xs-10">
    <a class="btn btn-default btn-block">right-buffer-*-10</a>
  </div>
  <div class="col-xs-6 left-buffer-xs-10">
    <a class="btn btn-default btn-block">left-buffer-*-10</a>
  </div>
</div>
<div class="row bottom-buffer-xs-10">
  <div class="col-xs-6 right-buffer-xs-15">
    <a class="btn btn-default btn-block">right-buffer-*-15</a>
  </div>
  <div class="col-xs-6 left-buffer-xs-15">
    <a class="btn btn-default btn-block">left-buffer-*-15</a>
  </div>
</div>
<div class="row bottom-buffer-xs-10">
  <div class="col-xs-6 right-buffer-xs-20">
    <a class="btn btn-default btn-block">right-buffer-*-20</a>
  </div>
  <div class="col-xs-6 left-buffer-xs-20">
    <a class="btn btn-default btn-block">left-buffer-*-20</a>
  </div>
</div>
<div class="row bottom-buffer-xs-10">
  <div class="col-xs-6 right-buffer-xs-30">
    <a class="btn btn-default btn-block">right-buffer-*-30</a>
  </div>
  <div class="col-xs-6 left-buffer-xs-30">
    <a class="btn btn-default btn-block">left-buffer-*-30</a>
  </div>
</div>
```
