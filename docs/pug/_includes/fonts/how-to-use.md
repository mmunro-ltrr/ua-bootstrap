## How to Use
The Milo font suite is available for official use by UA employees through
a license with FontShop. Please follow the instructions below to ensure
compliance with the license.

### **Instructions**

Before using the Milo font suite, please complete the <a href="https://brand.arizona.edu/font-license-agreement" target="_blank">font license
agreement</a> <span class="label
label-warning">Important</span>

For projects not using UA Bootstrap, follow these instructions on <a href="https://brand.arizona.edu/how-add-milo-your-website" target="_blank">how to add Milo to your website</a>.
Be aware that [UA Bootstrap reference links](index.html#ua-bootstrap-reference-links) come "pre-loaded"
with the Milo font suite. In other words, if you're using UA Bootstrap, font
installation instructions are not required.<span class="label
label-warning">Important</span>

### **Fallback Fonts**

Not all web browsers know about web fonts, and thus we must include consistent
fallback typefaces so that your visitor see the same font families when they
visit UA sites. Below is the official font stack for both Milo Sans and Milo
Serif.

**Serif**

```css
font-family: MiloSerifWeb, TimesNewRoman, "Times New Roman", Times, Baskerville,
Georgia, serif;
```

**Milo Sans Serif**

```css
font-family: MiloWeb, Verdana, Geneva, sans-serif;
```

### **Examples in CSS**

Below are two examples of how you would use the Milo fonts in your code.

**Example 1**

```css
h2 {
  font-family: MiloSerifWeb, TimesNewRoman, "Times New Roman", Times,
  Baskerville, Georgia, serif;
}
```
```html
<h2 class="msw">Hello World!</h2>
```

**Example 2**

```css
p { font-family: MiloWeb, Verdana, Geneva, sans-serif; }
```

```html
<p>Hello World!</p>
```

