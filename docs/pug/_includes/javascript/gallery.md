
<h1 id="gallery">Photo Gallery <small>modal.js</small></h1>

<h2 id="gallery-examples">Examples</h2>
<p>This provides a set of classes for the display of pictures in both thumbnail format, as well as a fullscreen carousel set onto the backdrop of a modal.</p>

<h3><strong>Live Demo</strong></h3>

<div class="example">
  <div class="container-fluid">
    <div class="row show-grid">
      <div class="col-xs-4">
        <p>Col-4</br>(Shown to demo grid alignment)</p>
      </div>
      <div class="col-xs-4">
        <p>Col-4</br>(Shown to demo grid alignment)</p>
      </div>
      <div class="col-xs-4">
        <p>Col-4</br>(Shown to demo grid alignment)</p>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="px-min py-min col-xs-6 col-sm-6 col-md-4 col-lg-3 col-xl-2" data-toggle="modal" data-target="#myGalleryModal">
          <a href="#myGallery" data-slide-to="0">
            <picture class="card-img img-responsive">
              <source srcset="img/gallery-demo/gallery-img-1-sm.jpg 1x">
              <img src="img/gallery-demo/gallery-img-1.jpg" alt="A picture of a hand holding a little mirror." title="">
            </picture>
          </a>
        </div>
        <div class="px-min py-min col-xs-6 col-sm-6 col-md-4 col-lg-3 col-xl-2" data-toggle="modal" data-target="#myGalleryModal">
          <a href="#myGallery" data-slide-to="1">
            <picture class="card-img img-responsive">
              <source srcset="img/gallery-demo/gallery-img-2-sm.jpg 1x">
              <img src="img/gallery-demo/gallery-img-2.jpg" alt="This is a panorama shot of the the University of Arizona main campus." title="">
            </picture>
          </a>
        </div>
        <div class="px-min py-min col-xs-6 col-sm-6 col-md-4 col-lg-3 col-xl-2" data-toggle="modal" data-target="#myGalleryModal">
          <a href="#myGallery" data-slide-to="2">
            <picture class="card-img img-responsive">
              <source srcset="img/gallery-demo/gallery-img-3-sm.jpg 1x">
              <img src="img/gallery-demo/gallery-img-3.jpg" alt="This is an image of a recent University of Arizona graduate recieving her diploma." title="">
            </picture>
          </a>
        </div>
        <div class="px-min py-min col-xs-6 col-sm-6 col-md-4 col-lg-3 col-xl-2" data-toggle="modal" data-target="#myGalleryModal">
          <a href="#myGallery" data-slide-to="3">
            <picture class="card-img img-responsive">
              <source srcset="img/gallery-demo/gallery-img-4-sm.jpg 1x">
              <img src="img/gallery-demo/gallery-img-4.jpg" alt="This is a picutre of a bunch of small reflective boxes." title="">
            </picture>
          </a>
        </div>
      </div>
    </div>
    <div class="row show-grid">
      <div class="col-xs-4">
        <p>Col-4</br>(Shown to demo grid alignment)</p>
      </div>
      <div class="col-xs-4">
        <p>Col-4</br>(Shown to demo grid alignment)</p>
      </div>
      <div class="col-xs-4">
        <p>Col-4</br>(Shown to demo grid alignment)</p>
      </div>
    </div>
  </div>
  <div class="modal fade modal-full modal-bg-dark container-fluid" tabindex="-1" id="myGalleryModal" role="dialog" aria-labelledby="myGalleryModalLabel">
    <div id="myGallery" class="carousel slide" data-interval="false" style="height:100%;">
      <button type="button" class="close text-white ua-gallery-close" data-dismiss="modal" aria-label="Close" style="font-size:30px;"><span aria-hidden="true"><i aria-hidden="true" class="ua-brand-x"></i></span></button>
      <div class="carousel-inner" style="height:98% !important;">
        <div class="item ua-gallery-item active">
          <div class="carousel-image">
            <picture>
              <img class="ua-gallery-carousel-image" src="img/gallery-demo/gallery-img-1.jpg" alt="This is a picture of a hand holding a little mirror." title="">
            </picture>
          </div>
          <div class="ua-gallery-caption bg-black text-white text-center">
            This is a picture of a hand holding a little mirror.
          </div>
        </div>
        <div class="item ua-gallery-item">
          <div class="carousel-image">
            <picture>
              <img class="ua-gallery-carousel-image" src="img/gallery-demo/gallery-img-2.jpg" alt="This is a panorama shot of the the University of Arizona main campus." title="">
            </picture>
          </div>
          <div class="ua-gallery-caption bg-black text-white text-center">
            This is a panorama shot of the the University of Arizona main campus.
          </div>
        </div>
        <div class="item ua-gallery-item">
          <div class="carousel-image">
            <picture>
              <img class="ua-gallery-carousel-image" src="img/gallery-demo/gallery-img-3.jpg" alt="This is an image of a recent University of Arizona graduate recieving her diploma." title="">
            </picture>
          </div>
          <div class="ua-gallery-caption bg-black text-white text-center">
            This is an image of a recent University of Arizona graduate recieving her diploma.
          </div>
        </div>
        <div class="item ua-gallery-item">
        <div class="carousel-image">
          <picture>
            <img class="ua-gallery-carousel-image" src="img/gallery-demo/gallery-img-4.jpg" alt="This is an image of a series of reflective boxes." title="">
          </picture>
        </div>
        <div class="ua-gallery-caption bg-black text-white text-center">
          I have no idea what this is. But it looks cool doesn't it?
        </div>
      </div>
    </div>
    <a class="left carousel-control ua-gallery-carousel-control" data-slide="prev" href="#myGallery" role="button">
      <i aria-hidden="true" class="carousel-next ua-brand-left-arrow"></i>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control ua-gallery-carousel-control" data-slide="next" href="#myGallery" role="button">
      <i aria-hidden="true" class="carousel-prev ua-brand-right-arrow"></i>
      <span class="sr-only">Next</span>
    </a>
  </div>
  </div>
</div>
```html
<div class="container-fluid">
    <div class="row">
      <div class="col-xs-12">
        <div class="px-min py-min col-xs-6 col-sm-6 col-md-4 col-lg-3 col-xl-2" data-toggle="modal" data-target="#myGalleryModal">
          <a href="#myGallery" data-slide-to="0">
            <picture class="card-img img-responsive">
              <source srcset="img/gallery-demo/gallery-img-1-sm.jpg 1x">
              <img src="img/gallery-demo/gallery-img-1.jpg" alt="A picture of a hand holding a little mirror." title="">
            </picture>
          </a>
        </div>
        <div class="px-min py-min col-xs-6 col-sm-6 col-md-4 col-lg-3 col-xl-2" data-toggle="modal" data-target="#myGalleryModal">
          <a href="#myGallery" data-slide-to="1">
            <picture class="card-img img-responsive">
              <source srcset="img/gallery-demo/gallery-img-2-sm.jpg 1x">
              <img src="img/gallery-demo/gallery-img-2.jpg" alt="This is a panorama shot of the the University of Arizona main campus." title="">
            </picture>
          </a>
        </div>
        <div class="px-min py-min col-xs-6 col-sm-6 col-md-4 col-lg-3 col-xl-2" data-toggle="modal" data-target="#myGalleryModal">
          <a href="#myGallery" data-slide-to="2">
            <picture class="card-img img-responsive">
              <source srcset="img/gallery-demo/gallery-img-3-sm.jpg 1x">
              <img src="img/gallery-demo/gallery-img-3.jpg" alt="This is an image of a recent University of Arizona graduate recieving her diploma." title="">
            </picture>
          </a>
        </div>
        <div class="px-min py-min col-xs-6 col-sm-6 col-md-4 col-lg-3 col-xl-2" data-toggle="modal" data-target="#myGalleryModal">
          <a href="#myGallery" data-slide-to="3">
            <picture class="card-img img-responsive">
              <source srcset="img/gallery-demo/gallery-img-4-sm.jpg 1x">
              <img src="img/gallery-demo/gallery-img-4.jpg" alt="This is a picutre of a bunch of small reflective boxes." title="">
            </picture>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade modal-full modal-bg-dark container-fluid" tabindex="-1" id="myGalleryModal" role="dialog" aria-labelledby="myGalleryModalLabel">
    <div id="myGallery" class="carousel slide" data-interval="false" style="height:100%;">
      <button type="button" class="close text-white ua-gallery-close" data-dismiss="modal" aria-label="Close" style="font-size:30px;"><span aria-hidden="true"><i aria-hidden="true" class="ua-brand-x"></i></span></button>
      <div class="carousel-inner" style="height:98% !important;">
        <div class="item ua-gallery-item active">
          <div class="carousel-image">
            <picture>
              <img class="ua-gallery-carousel-image" src="img/gallery-demo/gallery-img-1.jpg" alt="This is a picture of a hand holding a little mirror." title="">
            </picture>
          </div>
          <div class="ua-gallery-caption bg-black text-white text-center">
            This is a picture of a hand holding a little mirror.
          </div>
        </div>
        <div class="item ua-gallery-item">
          <div class="carousel-image">
            <picture>
              <img class="ua-gallery-carousel-image" src="img/gallery-demo/gallery-img-2.jpg" alt="This is a panorama shot of the the University of Arizona main campus." title="">
            </picture>
          </div>
          <div class="ua-gallery-caption bg-black text-white text-center">
            This is a panorama shot of the the University of Arizona main campus.
          </div>
        </div>
        <div class="item ua-gallery-item">
          <div class="carousel-image">
            <picture>
              <img class="ua-gallery-carousel-image" src="img/gallery-demo/gallery-img-3.jpg" alt="This is an image of a recent University of Arizona graduate recieving her diploma." title="">
            </picture>
          </div>
          <div class="ua-gallery-caption bg-black text-white text-center">
            This is an image of a recent University of Arizona graduate recieving her diploma.
          </div>
        </div>
        <div class="item ua-gallery-item">
        <div class="carousel-image">
          <picture>
            <img class="ua-gallery-carousel-image" src="img/gallery-demo/gallery-img-4.jpg" alt="An image of some reflective boxes." title="">
          </picture>
        </div>
        <div class="ua-gallery-caption bg-black text-white text-center">
          I have no idea what this is. But it looks cool doesn't it?
        </div>
      </div>
    </div>
    <a class="left carousel-control ua-gallery-carousel-control" data-slide="prev" href="#myGallery" role="button">
      <i aria-hidden="true" class="carousel-next ua-brand-left-arrow"></i>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control ua-gallery-carousel-control" data-slide="next" href="#myGallery" role="button">
      <i aria-hidden="true" class="carousel-prev ua-brand-right-arrow"></i>
      <span class="sr-only">Next</span>
    </a>
  </div>
  </div>
```
<h2 id="gallery-usage">Usage</h2>
<p>There are two chunks of code needed for each image to be a part of a gallery (example above).</p>
<p>First, every thumbnail image should be a <code>div</code> within a <code>row</code>.</br>
By default we use a thumbnail that is 500x350px.</p>
<p><b>NOTE:</b> The data-slide-to reference is what determines which fullscreen image matches with which thumbnail.


```html
<div class="right-buffer-xs-1 top-buffer-xs-1 bottom-buffer-xs-1 left-buffer-xs-1 col-xs-6 col-sm-6 col-md-4 col-lg-3 col-xl-2" data-toggle="modal" data-target="#myGalleryModal">
  <a href="#myGallery" data-slide-to="0">
    <picture class="card-img img-responsive">
      <source srcset="/path/to/your/image-thumbnail.jpg 1x">
      <img src="/path/to/your/image.jpg" alt="Alt text for your picture" title="">
    </picture>
  </a>
</div>
```
<p>Next you will build your fullscreen modal and carousel as seem in the live demo above.</p>
<p>Under the <code>carousel-image</code> div each image will need the below code.

```html
<div class="item ua-gallery-item active">
  <div class="carousel-image">
    <picture>
      <img class="ua-gallery-carousel-image" src="/path/to/your/image.jpg" alt="Image alt text" title="Image title text">
    </picture>
  </div>
  <div class="ua-gallery-caption bg-black text-white text-center">
    This is the caption of your image
  </div>
</div>
```


