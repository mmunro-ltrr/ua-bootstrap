module.exports = {
  /* Copy necessary files into `dist` */
  dist: (gulp, config) => () => {
    return gulp.src(config.buildDir + '/lib/**')
      .pipe(gulp.dest(config.distDir))
  }
}
