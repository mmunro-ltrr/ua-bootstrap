const fetch = require('node-fetch')
const p = process

const attachmentDefaults = {
  color: '#FF6347',
  author_name: 'uadigital/ua-bootstrap',
  author_link: 'https://bitbucket.org/uadigital/ua-bootstrap',
  text: p.env.COMMIT_MSG
}

function postMessage (channel, text, attachments) {
  const message = {
    'username': 'hexo',
    'icon_emoji': ':beryl:',
    channel,
    text,
    attachments
  }

  fetch(`https://hooks.slack.com/services/${p.env.SLACK_SECRET}`, { method: 'POST', body: JSON.stringify(message) })
}

const messages = {
  /* Hosted preview is available for review */
  previewReady () {
    const attachment = Object.assign({}, attachmentDefaults, {
      fallback: `Branch "${p.env.BRANCH}\` ready for review at: ${p.env.S3URL}/review/${p.env.BRANCH}`,
      title: `Review: ${p.env.BRANCH}`,
      title_link: `${p.env.S3URL}/review/${p.env.BRANCH}`
    })

    postMessage('#ua-bootstrap', undefined, [attachment])
  },

  /* Backstop tests are done and ready for review */
  backstop () {
    const attachment = Object.assign({}, attachmentDefaults, {
      fallback: `Visual Diff ready for review at: ${p.env.S3URL}/review/${p.env.BRANCH}/backstop_data/html_report/index.html`,
      title: `Visual Diff: ${p.env.BRANCH}`,
      title_link: `${p.env.S3URL}/review/${p.env.BRANCH}/backstop_data/html_report/index.html`
    })

    postMessage('#ua-bootstrap', undefined, [attachment])
  },

  /* WebAim is ready for reviewing the changes */
  webaim () {
    const attachment = Object.assign({}, attachmentDefaults, {
      fallback: `To check the result of this pull request in the WebAIM accessibility tool go here: http://wave.webaim.org/report#/${p.env.S3URL}/review/${p.env.BRANCH}/index.html`,
      title: `WebAIM Analysis: ${p.env.BRANCH}`,
      title_link: `http://wave.webaim.org/report#/${p.env.S3URL}/review/${p.env.BRANCH}`
    })

    postMessage('#accessibility', undefined, [attachment])
  }
}

const message = messages[process.argv.pop()]
try {
  message()
} catch (error) {
  console.error('Message not found')
}
