#!/bin/sh
#------------------------------------------------------------------------------
#
# bitbucket_autorelease.sh: push a tagged release to Bitbucket.
#
# Returns:
#   0 on success, 1 on any error.
#
# This expects to be within a clone of a Bitbucket-hosted Git repository for UA
# Bootstrap that already contains tags for various releases. It creates a
# temporary Git branch, finds the most recent tag, increments the version
# number, and uses Git to push to the Bitbucket repository, then uses the
# Bitbucket API to create a pull request from this, and finally merges the
# request, all without human interaction. It then builds out the complete
# UA Bootstrap distribution and updates artifacts elsewhere, including running
# the Node.js script that updates the UA Zen theme within the UA Quickstart
# Drupal distribution.
#------------------------------------------------------------------------------

set -e

#------------------------------------------------------------------------------
# Environment variables and semi-constants.

# Verbose logging (only turned off if the variable is both set and null).
: "${UABOOTSTRAP_VERBOSE=1}"

# Main branch name
: "${UABOOTSTRAP_MAINBRANCH:=master}"

#------------------------------------------------------------------------------
# Utility function definitions.

errorexit () {
  echo "** $1." >&2
  exit 1
}

# Show progress on STDERR if verbose logging is on.
if [ -n "$UABOOTSTRAP_VERBOSE" ]; then
  logmessage () {
    echo "$1..." >&2
  }
  normalexit () {
    echo "$1." >&2
    exit 0
  }
else
  logmessage () {
    return
  }
  normalexit () {
    exit 0
  }
fi

#------------------------------------------------------------------------------
# OAuth authentication for the Bitbucket API.
# It uses the Client Credentials Grant flow from RFC-6749 section 4.4
# https://tools.ietf.org/html/rfc6749#section-4.4
# Note that Bitbucket's implementation requires a pre-existing OAuth consumer,
# defined at the level of the general team or user account access management
# settings, with the “This is a private consumer” option checked, and the
# “Permissions” section pre-defining the most privileged scope the OAuth
# consumer will require (the scope on the access token request is just a check
# that the predefined scopes would allow it: it neither extends nor limits
# these).
# See:
# https://developer.atlassian.com/bitbucket/api/2/reference/meta/authentication
# https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html
# https://developer.atlassian.com/cloud/bitbucket/oauth-2/

# Global token (initialized to a bad value).
access_token='initial_invalid_setting'

# Obtain an access token, given client credentials.
#
# Parameters:
# $1 - Client ID (string).
# $2 - Client secret (string).
# $3 - Scopes expected for the access token (string).
setglobaltoken () {
  client_id=$1
  client_secret=$2
  scopes_expected=$3
  token_endpoint='https://bitbucket.org/site/oauth2/access_token'
  tokenfail='Could not obtain an OAuth access token'
  auth_resp=$(curl --silent \
    --request POST \
    --user "${client_id}:${client_secret}" \
    "$token_endpoint" \
    --data grant_type=client_credentials \
    --data scopes="$scopes_expected") \
    || errorexit "${tokenfail}, curl failed with status ${?}"
  [ -n "$auth_resp" ] \
    || errorexit "${tokenfail}, empty response from ${token_endpoint}"
  auth_errtext=$(echo "$auth_resp" | jq --raw-output '.error') \
    || errorexit "${tokenfail}, could not extract the error field in the response from ${token_endpoint}, '${auth_resp}''"
  [ "$auth_errtext" = 'null' ] \
    || errorexit "${tokenfail}, ${token_endpoint} sent the error '${auth_errtext}' in the response '${auth_resp}''"
  access_token=$(echo "$auth_resp" | jq --raw-output '.access_token') \
    || errorexit "${tokenfail}, could not extract the access_token field in the response from ${token_endpoint}, '${auth_resp}'"
  [ "$access_token" != 'null' ] \
    || errorexit "${tokenfail}, ${token_endpoint} did not include the access_token field in the response '${auth_resp}''"
}

#------------------------------------------------------------------------------
# Initial run-time error checking.

[ -n "$BITBUCKET_REPO_OWNER" ] \
  || errorexit "No Bitbucket repository owner specified"
[ -n "$BITBUCKET_REPO_SLUG" ] \
  || errorexit "No repository specified"
[ -n "$PR_CLIENT_ID" ] \
  || errorexit "Missing the pull request client ID from the authentication credentials"
[ -n "$PR_CLIENT_SECRET" ] \
  || errorexit "Missing the pull request client secret from the authentication credentials"
[ -n "$UAQS_REPO_CLIENT_ID" ] \
  || errorexit "Missing the UAQS repository update client ID from the authentication credentials"
[ -n "$UAQS_REPO_CLIENT_SECRET" ] \
  || errorexit "Missing the UAQS repository update client secret from the authentication credentials"

#------------------------------------------------------------------------------
# Global Git configuration.

git config --global user.name "$BITBUCKET_REPO_OWNER" \
  || errorexit "Could not set the Git user as ${BITBUCKET_REPO_OWNER}"
git config --global user.email "noreply@bitbucket.org" \
  || errorexit "Could not set the generic Bitbucket email address for the Git user"

#------------------------------------------------------------------------------
# Bitbucket API authentication.

logmessage 'Authenticating to Bitbucket for making a pull request'
setglobaltoken "$PR_CLIENT_ID" "$PR_CLIENT_SECRET" 'pullrequest:write'
scratchname=$(mktemp -t 'CI-AZDIGITAL-XXXXXXXXXXXXXXX') \
  || errorexit "Failed to create a temporary file name to re-use as a branch name"

#------------------------------------------------------------------------------
# Setup writable remote, and check out a uniquely named temporary local branch.

logmessage 'Checking out temporary branch'
bootstrap_repo_url="https://x-token-auth:${access_token}@bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}"
git remote set-url origin "$bootstrap_repo_url" \
  || errorexit "Could not switch to ${bootstrap_repo_url} as a writable Git remote origin repository"
oldtag=$(git describe --tags --always --abbrev=0) \
  || errorexit "Git crashed when trying to find an existing tag"
echo "$oldtag" | grep -q -E '.+\..+\..+' \
  || errorexit "The string '${oldtag}', recovered as an existing tag, does not look like a semver version specification"
scratchname=$(mktemp -t 'CI-AZDIGITAL-XXXXXXXXXXXXXXX') \
  || errorexit "Failed to create a temporary file name to re-use as a branch name"
scratch=$(basename "$scratchname") \
  || errorexit "Failed to make a scratch Git branch name from the filename ${scratchname}"
git checkout -b "$scratch" \
  || errorexit "Could not switch to the scratch Git branch ${scratch}"

#------------------------------------------------------------------------------
# The yarn package manager a makes a new auto-incremented tag and package.json.

logmessage 'Installing dependencies defined in the package.json file'
yarn \
  || errorexit "Could not complete a 'yarn install' to install the dependencies from the package.json file"
yarn version --prerelease \
  || errorexit "The yarn package manager could not auto-increment the ${oldtag} tag"
UABOOTSTRAP_NEW_TAG=$(git describe --tags --always --abbrev=0) \
  || errorexit "Git crashed when trying to find the updated tag"
export UABOOTSTRAP_NEW_TAG
echo "$UABOOTSTRAP_NEW_TAG" | grep -q -E '.+\..+\..+' \
  || errorexit "The string '${UABOOTSTRAP_NEW_TAG}', supposedly the updated tag, does not look like a semver version specification"
[ "$UABOOTSTRAP_NEW_TAG" != "$oldtag" ] \
  || errorexit "The most recent tag is still ${oldtag}"

#------------------------------------------------------------------------------
# Push the package.json change, then the new tag.

logmessage 'Pushing changes back to the origin'
git push origin "$scratch" \
  || errorexit "Couldn't push the updated package.json to a new temporary ${scratch} branch"
git push origin "$UABOOTSTRAP_NEW_TAG" \
  || errorexit "Couldn't push the new ${UABOOTSTRAP_NEW_TAG} tag"

#------------------------------------------------------------------------------
# Make a pull request from the scratch branch via the Bitbucket API.

logmessage 'Making a pull request using the temporary branch'
prfail='Could not create the Bitbucket pull request'
pr_endpoint="https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/pullrequests"
pr_create_json=$(jq --null-input \
  --arg prtitle "Automatically tag UA Bootstrap for ${UABOOTSTRAP_NEW_TAG}" \
  --arg sourcebranch "$scratch" \
  --arg destbranch "$UABOOTSTRAP_MAINBRANCH" \
  '{title:$prtitle, source:{branch:{name:$sourcebranch}}, destination:{branch:{name:$destbranch}}, close_source_branch:true}') \
  || errorexit "${prfail}, failed making the JSON string for the Bitbucket API to use"
pr_resp=$(curl "$pr_endpoint" \
  --request POST \
  --header "Authorization: Bearer ${access_token}" \
  --header 'Content-Type: application/json' \
  --data "$pr_create_json") \
  || errorexit "${prfail}, curl failed with status ${?}"
[ -n "$pr_resp" ] \
  || errorexit "${prfail}, empty response from ${pr_endpoint}"
pr_errtext=$(echo "$pr_resp" | jq --raw-output '.error') \
  || errorexit "${prfail}, could not extract the error field in the response from ${pr_endpoint}, '${pr_resp}''"
[ "$pr_errtext" = 'null' ] \
  || errorexit "${prfail}, ${pr_endpoint} sent the error '${pr_errtext}' in the response '${pr_resp}'"
pr_id=$(echo "$pr_resp" | jq --raw-output '.id') \
  || errorexit "${prfail}, could not extract the PR id field in the response from ${pr_endpoint}, '${pr_resp}'"
[ "$pr_id" != 'null' ] \
  || errorexit "${prfail}, ${pr_endpoint} did not include the id field in the response '${pr_resp}''"

#------------------------------------------------------------------------------
# Self-approve the pull request via the Bitbucket API.

logmessage 'Immediately approving the new pull rquest'
approvefail="Could not approve the Bitbucket pull request ${pr_id}"
approve_endpoint="${pr_endpoint}/${pr_id}/approve"
approve_resp=$(curl "${approve_endpoint}" \
  --request POST \
  --header "Authorization: Bearer ${access_token}") \
  || errorexit "${approvefail}, curl failed with status ${?}"
[ -n "$approve_resp" ] \
  || errorexit "${approvefail}, empty response from ${approve_endpoint}"
approve_errtext=$(echo "$approve_resp" | jq --raw-output '.error') \
  || errorexit "${approvefail}, could not extract the error field in the response from ${approve_endpoint}, '${approve_resp}'"
[ "$approve_errtext" = 'null' ] \
  || errorexit "${approvefail}, ${approve_endpoint} sent the error '${approve_errtext}' in the response '${approve_resp}'"

#------------------------------------------------------------------------------
# Merge the pull request onto the main branch via the Bitbucket API.

logmessage 'Merging the pull request onto the master branch'
mergefail="Could not merge the Bitbucket pull request ${pr_id}"
merge_endpoint="${pr_endpoint}/${pr_id}/merge"
merge_json=$(jq --null-input \
  --arg commitmessage "Automated release for ${UABOOTSTRAP_NEW_TAG}" \
  '{message:$commitmessage, close_source_branch:true, merge_strategy:"merge_commit"}') \
  || errorexit "${mergefail}, failed making the JSON string for the Bitbucket API to use"
raw_resp=$(curl "${merge_endpoint}" \
  --request POST \
  --header "Authorization: Bearer ${access_token}" \
  --header 'Content-Type: application/json' \
  --data "$merge_json") \
  || errorexit "${mergefail}, curl failed with status ${?}"
merge_resp=$(echo "$raw_resp" | tr '[:cntrl:]' ' ') \
  || errorexit "${mergefail}, could not sanitize the raw response '${raw_resp}'"
[ -n "$merge_resp" ] \
  || errorexit "${mergefail}, empty response from ${merge_endpoint}"
merge_errtext=$(echo "$merge_resp" | jq --raw-output '.error') \
  || errorexit "${mergefail}, could not extract the error field in the response from ${merge_endpoint}, '${merge_resp}'"
[ "$merge_errtext" = 'null' ] \
  || errorexit "${mergefail}, ${merge_endpoint} sent the error '${merge_errtext}' in the response '${merge_resp}'"

#------------------------------------------------------------------------------
#  The Gulp task runner builds out the full UA Bootstrap distribution content.

logmessage 'Building out the full UA Bootstrap distribution'
gulp build:dist \
  || errorexit "Failed to build out the UA Bootstrap distribution content"

#------------------------------------------------------------------------------
#  A Node.js script invoked by yarn pushes updates to a UA Quickstart theme.

logmessage 'Pushing the updates to the theme in a UA Quickstart repository'
setglobaltoken "$UAQS_REPO_CLIENT_ID" "$UAQS_REPO_CLIENT_SECRET" 'repository:write'
UABOOTSTRAP_UAQS_REPO="https://x-token-auth:${access_token}@bitbucket.org/tobiashume/ua_quickstart"
export UABOOTSTRAP_UAQS_REPO
yarn run update-uazen \
  || errorexit "Failed to update the UA Zen theme with the new UA Bootstrap changes"

normalexit "Tagged a new ${UABOOTSTRAP_NEW_TAG} UA Bootstrap release, and updated the UA Quickstart UA Zen theme"