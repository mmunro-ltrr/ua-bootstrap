#!/usr/bin/env node

const fsPromises = require('fs').promises;
const path = require('path');
const git = require('simple-git/promise');
const { argv } = require('yargs');

const p = process;

const newTag = p.env.UABOOTSTRAP_NEW_TAG;
const verbosity = p.env.UABOOTSTRAP_VERBOSE || '1';
const uaqsURL = p.env.UABOOTSTRAP_UAQS_REPO || 'git@bitbucket.org:ua_drupal/ua_quickstart.git';

function handleGitOutput(command, stdout, stderr) {
  stdout.pipe(p.stdout);
  stderr.pipe(p.stderr);
}

const normalLogger = {
  indentLevel: 0,
  message(mess) {
    // eslint-disable-next-line no-console
    console.log(mess);
  },
  debugMessage() {},
  paddedName(name) {
    return ' '.repeat(this.indentLevel) + name;
  },
  formattedEntry(name) {
    return this.paddedName(name);
  },
  formattedDir(dirName, entries) {
    const dirHeader = `${this.paddedName(dirName)}/ . . . . .`;
    return [dirHeader].concat(entries).join('\n');
  },
  deeper() {
    return Object.assign({}, this, { indentLevel: this.indentLevel + 2 });
  },
};

const quietLogger = {
  message() {},
  debugMessage() {},
  paddedName() {},
  formattedEntry() {},
  formattedDir() {},
  deeper() { return this; },
};

const debugLogger = Object.assign(
  {},
  normalLogger,
  {
    debugMessage(mess) {
      // eslint-disable-next-line no-console
      console.log(this.paddedName(mess));
    },
  },
);

const topLog = ((level) => {
  switch (level) {
    case '0':
      return quietLogger;
    case '2':
      return debugLogger;
    default:
      return normalLogger;
  }
})(verbosity);

p.on('warning', (warning) => {
  if (warning.name === 'ExperimentalWarning') {
    return;
  }
  topLog.message(warning.name);
  topLog.message(warning.message);
  topLog.message(warning.stack);
});

const errorExit = (message) => {
  throw Error(message);
};

const joinedPath = (root, pathList) => (pathList).reduce(
  (fullPath, currentDir) => path.join(fullPath, currentDir),
  root,
);

const purgeTree = (victim, log) => fsPromises.readdir(victim, { withFileTypes: true })
  .then(entries => entries.map((entry) => {
    const entryPath = path.join(victim, entry.name);
    if (entry.isDirectory()) {
      return purgeTree(entryPath, log.deeper());
    }
    return fsPromises.unlink(entryPath);
  }))
  .then(hitList => Promise.all(hitList))
  .then(() => fsPromises.rmdir(victim))
  .catch(errmes => log.message(errmes))
  .then(() => fsPromises.stat(victim))
  .then(
    () => errorExit(`Directory still exists: ${victim}`),
    () => log.debugMessage(`Deleted OK: ${victim}`),
  );

const breakageHandler = (entry, name, log) => (errmes) => {
  const badName = `BROKEN ${name}`;
  log.message(`Problem processing ${entry} ${name}: ${errmes}`);
  return log.formattedEntry(badName);
};

const doFileEntry = (
  name, entryPath, dstTree, log,
) => fsPromises.copyFile(entryPath, path.join(dstTree, name))
  .then(() => log.formattedEntry(name))
  .catch(breakageHandler('file', name, log));

const doSymlinkEntry = (
  name, entryPath, dstTree, log,
) => fsPromises.readlink(entryPath)
  .then(resolvedLink => fsPromises.symlink(resolvedLink, path.join(dstTree, name)))
  .then(() => log.formattedEntry(name))
  .catch(breakageHandler('link', name, log));

const makeEntryProcessor = (srcTree, dstTree, parents, log, doDirEntry) => (entry) => {
  const entryPath = path.join(srcTree, entry.name);
  if (entry.isFile()) {
    return doFileEntry(entry.name, entryPath, dstTree, log);
  }
  if (entry.isSymbolicLink()) {
    return doSymlinkEntry(entry.name, entryPath, dstTree, log);
  }
  if (entry.isDirectory()) {
    return doDirEntry(entry.name, parents, log);
  }
  return errorExit(`Cannot process ${entry.name} in ${srcTree}`);
};

const makeDirProcessor = (srcRoot, dstRoot) => {
  const doDirEntry = (dirName, parents, log) => {
    const nextParents = parents.concat([dirName]);
    const nextLog = log.deeper();
    const srcTree = joinedPath(srcRoot, nextParents);
    const dstTree = joinedPath(dstRoot, nextParents);
    return fsPromises.mkdir(dstTree, { mode: 0o755 })
      .then(() => fsPromises.readdir(srcTree, { withFileTypes: true }))
      .then(entries => entries
        .map(makeEntryProcessor(
          srcTree, dstTree, nextParents, nextLog, doDirEntry,
        )))
      .then(dirList => Promise.all(dirList))
      .then(subTreeLog => log.formattedDir(dirName, subTreeLog))
      .catch(breakageHandler('dir', dirName, log));
  };
  return doDirEntry;
};

const doTopDir = (srcRoot, dstRoot, log, doDirEntry) => fsPromises.mkdir(dstRoot, { mode: 0o755 })
  .then(() => fsPromises.readdir(srcRoot, { withFileTypes: true }))
  .then(entries => entries
    .map(makeEntryProcessor(
      srcRoot, dstRoot, [], log, doDirEntry,
    )))
  .then(dirList => Promise.all(dirList))
  .then(subTreeLog => log.message(subTreeLog.join('\n')))
  .catch(breakageHandler('dir', srcRoot, log));

const updateUaZenCommonInc = async (uaZenPath) => {
  const commonIncPath = path.resolve(uaZenPath, 'includes', 'common.inc');
  const regex = new RegExp(/'UA_ZEN_UA_BOOTSTRAP_STABLE_VERSION',\s*'[^']*'/, 'g');
  try {
    const commonInc = await fsPromises.readFile(commonIncPath, 'utf8');
    const updatedCommonInc = commonInc.replace(regex, `'UA_ZEN_UA_BOOTSTRAP_STABLE_VERSION', '${newTag}'`);
    return fsPromises.writeFile(commonIncPath, updatedCommonInc, 'utf8');
  } catch (err) {
    return errorExit(`Could not update to ${newTag} in ${commonIncPath}`);
  }
};

async function main() {
  if (!newTag) {
    errorExit('UABOOTSTRAP_NEW_TAG environment variable must be defined.');
  }
  const branchName = `ua-bootstrap-${newTag}`;
  const commitMessage = `Updated UA Zen with UA Bootstrap ${newTag}.`;
  const themePathList = ['themes', 'custom', 'ua_zen'];
  const uaqsPath = path.resolve('ua_quickstart');
  const uaZenPath = joinedPath(uaqsPath, themePathList);

  topLog.message('Cloning UA Quickstart repository...');
  await git()
    .outputHandler(handleGitOutput)
    .clone(uaqsURL, uaqsPath);
  await git(uaqsPath)
    .outputHandler(handleGitOutput)
    .checkoutLocalBranch(branchName);

  topLog.message(`Updating UA Zen with UA Bootstrap ${newTag}...`);
  const source = path.resolve('dist');
  const destination = path.resolve(uaZenPath, 'ua-bootstrap');
  await Promise.all([
    purgeTree(destination, topLog),
    updateUaZenCommonInc(uaZenPath),
  ]);
  await doTopDir(source, destination, topLog, makeDirProcessor(source, destination));

  topLog.message('Staging UA Zen updates...');
  await git(uaqsPath)
    .outputHandler(handleGitOutput)
    .add(themePathList.join(path.sep));
  topLog.message('Committing changes...');
  await git(uaqsPath)
    .outputHandler(handleGitOutput)
    .commit(commitMessage);

  if (argv.push) {
    topLog.message('Pushing branch to origin...');
    await git(uaqsPath)
      .outputHandler(handleGitOutput)
      .push(['-u', '-f', 'origin', branchName]);
  }
}

main()
  .catch(
    (error) => {
      // eslint-disable-next-line no-console
      console.error(error);
      p.exit(1);
    },
  );
